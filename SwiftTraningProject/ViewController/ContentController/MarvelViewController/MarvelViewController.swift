//
//  MarvelViewController.swift
//  SwiftTraningProject
//
//  Created by 吉祥具 on 2017/5/20.
//  Copyright © 2017年 吉祥具. All rights reserved.
//

import Foundation
import UIKit

class MarvelViewController: BaseViewController {
    
    @IBOutlet weak var marvelTestBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//TODO: 初始化畫面元件
    func initLayout(){
        //初始化按鈕事件
        self.marvelTestBtn.addTarget(self, action: #selector(marvelTestBtn_Press(button:)), for: .touchUpInside)
    }

//TODO: 定義按鈕點擊事件
    @objc func marvelTestBtn_Press(button:UIButton){
        
    }
    
}
