//
//  MainTabViewController.swift
//  SwiftTraningProject
//
//  Created by 吉祥具 on 2017/5/19.
//  Copyright © 2017年 吉祥具. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.initLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//TODO: 初始化tabBar的樣式
    func initLayout(){
        self.tabBar.backgroundColor = UIColor.clear
        
        self.selectedIndex=0 //初始化選到的tab index
    }
    
}

