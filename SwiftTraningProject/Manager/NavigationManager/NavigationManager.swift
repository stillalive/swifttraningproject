//
//  NavigationManager.swift
//  SwiftTraningProject
//
//  Created by 吉祥具 on 2017/5/20.
//  Copyright © 2017年 吉祥具. All rights reserved.
//

import Foundation
import UIKit

class NavigationManager : NSObject {
    
    private static var mInstance : NavigationManager?
    
    private var tabarVC : MainTabViewController?
    
    let localizer = LocalizationManager.shareInstance()
    
//TODO: Singleton create
    static func shareInstance() -> NavigationManager{
        if (mInstance == nil) {
            mInstance=NavigationManager()
            mInstance!.createTabBar()
        }
        
        return mInstance!
    }

//TODO: tabarView Create
    func createTabBar(){
        //create tabBar item 
        let marvelViewController = MarvelViewController()
        marvelViewController.tabBarItem = UITabBarItem.init(title: localizer.getString(key: "Marvel_Tab", tableSet: nil), image: UIImage.init(named: "saber_tab_unclick"), selectedImage:  UIImage.init(named: "saber_tab_click"))
        
        let youtubeViewController = YoutubeViewController()
        youtubeViewController.tabBarItem = UITabBarItem.init(title: localizer.getString(key: "Youtube_Tab", tableSet: nil), image: UIImage.init(named: "shiki_tab_unclick"), selectedImage: UIImage.init(named: "shiki_tab_click"))
        
        //tabBarViewController initiate
        tabarVC = MainTabViewController()
        tabarVC!.viewControllers=[marvelViewController,youtubeViewController]
    }

//TODO: 回傳建好的tabBarVC
    func getTabBarVC() -> MainTabViewController{
        return tabarVC!
    }
    
}
